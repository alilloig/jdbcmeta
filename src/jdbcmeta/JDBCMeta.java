/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcmeta;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class JDBCMeta {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    //Si utilizáis el driver de mysql
    //Class.forName("com.mysql.jdbc.Driver");
    String db = "NBA";
    Connection con;
    ResultSet result;
    
    final String tableCatalog = "TABLE_CAT";
    final String tableSchema = "TABLE_SCHEM";
    final String tableName = "TABLE_NAME";
    final String tableType = "TABLE_TYPE";
    
    try {
      con = DriverManager.getConnection(
              "jdbc:mysql://localhost?serverTimezone=UTC","alilloig","sql");
      System.out.println("Conexion establecida");
      DatabaseMetaData dbmeta = con.getMetaData();
      
      String name = dbmeta.getDatabaseProductName();
      String driver = dbmeta.getDriverName();
      String url = dbmeta.getURL();
      String user = dbmeta.getUserName();
      System.out.println("DB type: "+name+", driver: "+driver+", url: "+url+", user: "+user);
      
      result = dbmeta.getTables(null, null, null, null);
      while (result.next()){
        String catalog = result.getString(tableCatalog);
        String schema = result.getString(tableSchema);
        String tName = result.getString(tableName);
        String type = result.getString(tableType);
        System.out.println("Tabla "+tName+"; catalogo: "+catalog+", schema: "+schema+", tipo: "+type);
      }
      
      
      result.close();
      con.close();
    } catch (SQLException ex) {
      System.out.println("Error en la conexion con "+db+": "+ex.toString());
    }  
  }
}
